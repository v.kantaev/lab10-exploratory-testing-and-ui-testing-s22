package com.deltation.sqrs10;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestAssetStore {

    WebDriver driver;
    private final String URL = "https://assetstore.unity.com/";

    @BeforeClass
    public void setUp() {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver = new FirefoxDriver(options);
        driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
        driver.get(URL);
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testAssetStore() {
        Assert.assertEquals(driver.getTitle(), "Unity Asset Store - The Best Assets for Game Making");
        
        WebElement searchBar = driver.findElement(By.xpath("//*[@id=\"Search/V3Controller\"]/div/div/div/div/div[1]/div/div[2]/div[1]/div/div[2]/div/div[1]/form/div/input"));
        searchBar.sendKeys("odin inspector");
        searchBar.submit();

        Assert.assertEquals(driver.getTitle(), "odin inspector - Asset Store");

        WebElement video = driver.findElement(By.xpath("//*[@id=\"main-layout-scroller\"]/div/div[1]/div[2]/div/div/div/div/div[2]/div[1]/div/div[3]/div/div[1]/a/div/iframe"));
        String videoUrl = video.getAttribute("src");
        Assert.assertEquals(videoUrl, "https://www.youtube.com/embed/5TbhbU0HATQ?feature=oembed");
        
        WebElement publisherName = driver.findElement(By.xpath("//*[@id=\"main-layout-scroller\"]/div/div[1]/div[2]/div/div/div/div/div[2]/div[1]/div/div[4]/div/div/div/div/div/div/div/a"));
        Assert.assertEquals(publisherName.getText(), "SIRENIX");
    }
}
